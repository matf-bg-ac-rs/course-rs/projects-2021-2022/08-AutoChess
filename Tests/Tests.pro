TEMPLATE    = subdirs
SUBDIRS     =  \
              GraphTest \
              UnitPoolTest


SOURCES +=  ../Autochess/code/graph.cpp \
            ../Autochess/code/unitpool.cpp

HEADERS +=  ../Autochess/code/graph.h
            ../Autochess/code/unitpool.h

