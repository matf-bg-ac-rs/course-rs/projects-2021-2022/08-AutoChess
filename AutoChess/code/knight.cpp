#include "knight.h"
#include "game.h"

Knight::Knight(Team team, quint32 level, QGraphicsItem *parent)
    : Unit(team, level, UnitType::sunknight, parent), _highlight(false)
{
    _hlPixmap = QPixmap(":/resources/images/sunKnightHL.png");
    _originalPixmap = QPixmap(":/resources/images/sunKnight.png");

    setImage();
    loadStats("Knight");
    hpBar = new HpBar(this, _team, _maxHp, _hp);
    manaBar = new ManaBar(this, _team, _maxMana, _mana);
    giveStatsForLevel();
    Game::game()->addToScene(hpBar);
    Game::game()->addToScene(manaBar);
    hpBar->setVisible(false);
    manaBar->setVisible(false);
}

void Knight::drawUnit()
{
    setZValue(ZValues::zUnit);
    Game::game()->addToScene(this);
    if(_team == Team::top && Game::game()->getGameMode() == HardMode::ON)
        hideUnit();
}

void Knight::castSpell()
{
    if(!hasTarget() || _silenced)
        return;
    _mana = 0;
    manaBar->setMana(0);
    bool killed = false;
    particleManager->receiveParticle(ParticleType::spell, _type, pos());
    _target->particleManager->receiveParticle(ParticleType::received, _type, _target->pos());
    _target->particleManager->receiveParticle(ParticleType::blood, _target->_type, _target->pos());
    killed = _target->takeDamage(_spellDamage);
    damageBar->addDamageDealt(_damage);

    if (killed) {
        _target->particleManager->clearParticles();
        _target->particleManager->clearDisableParticles();
        _target = nullptr;
    }
}

void Knight::giveStatsForLevel()
{
    _maxHp = _hpPerLevel * (_level + 1);
    _hp = _maxHp;
    hpBar->setMaxHp(_maxHp);
    _originalSpellDamage = _spellDamagePerLevel * (_level + 1);
    _spellDamage = _originalSpellDamage;
    updateSpellText();
    _damage = _dmgPerLevel * (_level + 1);
}

void Knight::updateSpellText()
{
    _spellText = "Sun Knight calls upon the power of sun to smite his target for " + QString::number(_spellDamage) + " damage";
}
