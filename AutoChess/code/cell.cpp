#include "cell.h"
#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOption>
#include <QString>
#include <QDrag>
#include <QMimeData>
#include <QGraphicsSceneEvent>
#include "game.h"


extern Cell* selectedCell;

Cell::Cell(QGraphicsObject* parent)
    : QGraphicsObject(parent)
{
    setParent(parent);
    _taken = false;
    setZValue(ZValues::zCell);
    _location = Location::unused;
}

QRectF Cell::boundingRect() const
{
    return QRectF(0, 0, 96, 96);
}

void Cell::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    Q_UNUSED(option)

    painter->setBrush(_color);
    painter->drawRect(0, 0, 96, 96);

}

void Cell::mousePressEvent(QGraphicsSceneMouseEvent * event)
{
    Q_UNUSED(event)

    if (getLocation() == Location::board) {
        selectedCell = this;
        emit CellClicked();
        //TODO:salji signal stats da se obrise
    }

    update();

}

void Cell::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
}

void Cell::setColor(QColor color)
{
    _color = color;
}

void Cell::setOriginalColor(QColor originalColor)
{
    _originalColor = originalColor;
}

void Cell::setTaken(bool taken)
{

    /// TODO MOZDA LEPSE NAPISATI
    if (getLocation() == Location::board) {
        //Board* board = static_cast<Board*>(parent());
        Game::game()->board->graph->matrix[_row][_col] = taken;
    }
    _taken = taken;
}

bool Cell::isTaken()
{
    return _taken;
}

Location Cell::getLocation() const
{
    return _location;
}

void Cell::setLocation(Location newLocation)
{
    _location = newLocation;
}
