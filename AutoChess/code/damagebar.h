#ifndef DAMAGEBAR_H
#define DAMAGEBAR_H

#include "utils.h"
#include <QGraphicsObject>

class DamageBar : public QGraphicsObject
{
public:
    DamageBar(QGraphicsObject* parent, Team team, QString name, QString level);
    ~DamageBar();

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *) override;

    void addDamageDealt(qint32 damageDealt);
    qint32 damageDealt() const;
    void setMaxDamageDealt(qint32 newMaxDamageDealt);
    void resetDamageDealt();

    void updateText();
    void setTextPos();

    void setLvl(QString level);
private:
    Team _team;
    qint32 _damageDealt;
    qint32 _maxDamageDealt;
    QColor _color;
    QString _name;
    QString _lvl;
    QGraphicsTextItem* _text;
    QGraphicsTextItem* _damageText;
};

#endif // DAMAGEBAR_H
