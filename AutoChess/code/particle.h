﻿#ifndef PARTICLE_H
#define PARTICLE_H

#include <QGraphicsObject>
#include <QGraphicsItem>
#include "utils.h"
#include <vector>

class Particle : public QGraphicsObject
{
    Q_OBJECT
public:
    Particle(ParticleType particleType, UnitType unitType, QGraphicsObject* parent = 0);

    void setImage();
    void drawParticle();
    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;

    //pixmap

    QPixmap _pixmap;
    QPixmap _scaledPixmap;

    ParticleType getParticleType() const;
    void setParticleType(ParticleType newParticleType);

    UnitType getUnitType() const;
    void setUnitType(UnitType newUnitType);

private:
    ParticleType _particleType;
    UnitType _unitType;
    qint32 _sideLen;


};

#endif // PARTICLE_H
