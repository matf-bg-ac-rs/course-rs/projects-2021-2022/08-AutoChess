#include "board.h"
#include "game.h"
#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOption>
#include <QRandomGenerator>
#include <algorithm>
#include <QDebug>

#include <cmath>

Board::Board()
    : color(QColor(135,206,250,0))
    , originalColor(QColor(135,206,250,0))
{
    graph = new Graph();
    setZValue(ZValues::zBoard);
}

QRectF Board::boundingRect() const
{
    return QRectF(0, 0, Cell::cellSideLen()*8, Cell::cellSideLen()*8);

}

void Board::drawBoard()
{
    setPos(400, -85);

    for (auto i = 0; i < 8; i++) {
        for (auto j = 0; j < 8; j++) {
            cells[i][j] = new Cell(this);
            Cell* cell = cells[i][j];
            cells[i][j]->_row =i;
            cells[i][j]->_col =j;
            cell->moveBy(x() + Cell::cellSideLen()*i, y() + Cell::cellSideLen()*j);
            cell->setColor(color);
            cell->setOriginalColor(originalColor);
            cell->setLocation(Location::board);
            Game::game()->addToScene(cells[i][j]);
        }
    }
    Game::game()->addToScene(this);
}

void Board::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    Q_UNUSED(option)

    painter->setBrush(color);
    painter->drawPixmap(QRect(0, 0, Cell::cellSideLen()*8, Cell::cellSideLen()*8),
                        QPixmap(":/resources/images/boardPix.png"));

}

void Board::addToBoard(Unit* selectedUnit)
{
    if(selectedUnit->_team == Team::bottom)
    {
        if(std::find(bottomUnits.begin(), bottomUnits.end(), selectedUnit) == bottomUnits.end())
            bottomUnits.append(selectedUnit);
    }
    else
    {
        if(std::find(topUnits.begin(), topUnits.end(), selectedUnit) == topUnits.end()){
            topUnits.append(selectedUnit);
            if(Game::game()->getGameMode() == HardMode::ON)
                selectedUnit->hideUnit();
        }
    }

}

void Board::removeFromBoard(Unit* unit)
{
    auto unitIt = std::find(bottomUnits.begin(), bottomUnits.end(), unit);
    if (unitIt == bottomUnits.end())
        return;

    quint32 d = std::distance(bottomUnits.begin(), unitIt);

    bottomUnits.remove(d);
    unit->getParentCell()->setTaken(false);
    Game::game()->economy->units--;
    Game::game()->economy->updateUnitsNum();

}

void Board::showEnemyUnits()
{
    for(auto unit : topUnits)
    {
        unit->showUnit();
    }
}
