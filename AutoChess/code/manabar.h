#ifndef MANABAR_H
#define MANABAR_H

#include "utils.h"
#include <QGraphicsObject>

class ManaBar : public QGraphicsObject
{
public:
    ManaBar(QGraphicsObject* parent, Team team, qint32 maxMana, qint32 mana);
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *) override;
    void setMaxMana(qint32 newMaxMana);

    qint32 mana() const;
    void addMana(qint32 manaVal);
    void setMana(qint32 val);

private:
    Team _team;
    qint32 _maxMana;
    qint32 _mana;
    QColor _color;
};

#endif // MANABAR_H
