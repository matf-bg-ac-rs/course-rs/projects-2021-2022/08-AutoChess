#include "particle.h"
#include <QGraphicsScene>
#include <QPainter>


Particle::Particle(ParticleType particleType, UnitType unitType, QGraphicsObject* parent)
    : QGraphicsObject(parent)
{
    _particleType = particleType;
    _unitType = unitType;

    setZValue(ZValues::zParticle);
    setVisible(false);
    _sideLen = 25;

    if (particleType == ParticleType::blood)
    {
        _pixmap = QPixmap(":/resources/images/blood.png");
        _sideLen = 50;
        setZValue(ZValues::zBloodParticle);
    }
    else if (particleType == ParticleType::heal)
    {
        _pixmap = QPixmap(":/resources/images/heal.png");
        _sideLen = 50;
        setZValue(ZValues::zBloodParticle);
    }
    else if(particleType == ParticleType::spell)
    {
        _sideLen = 50;
        switch(unitType)
        {
            case UnitType::necromage :
                _pixmap = QPixmap(":/resources/images/soulRip.png");
            break;
            case UnitType::elvenranger :
                _pixmap = QPixmap(":/resources/images/weakSpot.png");
            break;
            default:
                _pixmap = QPixmap(":/resources/images/sunknightSpell.png");
        }
    }
    else if(particleType == ParticleType::disable)
    {
        _sideLen = 45;
        setZValue(ZValues::zDisableParticle);
        switch(unitType)
        {
            case UnitType::dreadherald :
                _pixmap = QPixmap(":/resources/images/silence.png");
            break;
            default:
                _pixmap = QPixmap(":/resources/images/stun.png");
        }
    }
    else
    {
        switch(unitType) {
            case UnitType::dreadherald :
                _pixmap = QPixmap(":/resources/images/heraldBolt.png");
            break;
            case UnitType::elvenranger :
                _pixmap = QPixmap(":/resources/images/arrow.png");
            break;
            case UnitType::horseman :
                _pixmap = QPixmap(":/resources/images/sword.png");
            break;
            case UnitType::necromage :
                _pixmap = QPixmap(":/resources/images/necroBolt.png");
            break;
            case UnitType::reaper :
                _pixmap = QPixmap(":/resources/images/scythe.png");
            break;
            case UnitType::philosopher :
                _pixmap = QPixmap(":/resources/images/philosopherProjektil.png");
            break;
            default:
                _pixmap = QPixmap(":resources/images/sunSword.png");
        }
    }


    setImage();
}

ParticleType Particle::getParticleType() const
{
    return _particleType;
}

void Particle::setParticleType(ParticleType newParticleType)
{
    _particleType = newParticleType;
}

UnitType Particle::getUnitType() const
{
    return _unitType;
}

void Particle::setUnitType(UnitType newUnitType)
{
    _unitType = newUnitType;
}

void Particle::setImage()
{

    _scaledPixmap = QPixmap(_pixmap.scaled(_sideLen, _sideLen, Qt::KeepAspectRatio));
}
void Particle::drawParticle()
{

}

QRectF Particle::boundingRect() const {

    return QRectF(0, 0, _sideLen, _sideLen);
}

void Particle::paint(QPainter *painter,
           const QStyleOptionGraphicsItem *option,
           QWidget *) {

    Q_UNUSED(option)
    painter->drawPixmap(0, 0, _sideLen, _sideLen, _scaledPixmap);

}
