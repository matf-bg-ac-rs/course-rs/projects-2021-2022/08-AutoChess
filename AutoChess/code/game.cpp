#include "game.h"
#include <iostream>
#include <QDebug>
#include <QString>
#include <QMessageBox>
#include <QPushButton>

#include <QFileInfo>

#include <iostream>

extern Unit* selectedUnit;
extern Cell* selectedCell;

Game* Game::instance = 0;
int Game::count = 0;


class MouseClickEater : public QObject {
public:
    MouseClickEater(QObject* parent) {
        setParent(parent);
    }
    bool eventFilter(QObject * obj, QEvent* event)
    {
        if (event->type() == QEvent::MouseButtonPress) {

            if (selectedUnit != nullptr) {
                selectedUnit->deselectUnit();
            }
            selectedCell = nullptr;
            return true;
        } else {
            return QObject::eventFilter(obj, event);
        }
    }
};
Game* Game::game()
{
    if (!instance)
        instance = new Game();
    return instance;
}

Game::Game(QWidget* parent)
    : QGraphicsView(parent),
      _sceneWidth(1680),
      _sceneHeight(900),
      _hardMode(HardMode::OFF),
      _phase(Phase::shop),
      _shopLocked(false),
      _maxRounds(5),
      _roundsWon(0),
      _roundsEnemyWon(0),
      _savePath("gameSummary.json")
{
    count++;
    scene = new QGraphicsScene();
    scene->setSceneRect(-100, -100, _sceneWidth, _sceneHeight);
    scene->setBackgroundBrush(QColor(64,160,13));
    scene->setItemIndexMethod(QGraphicsScene::NoIndex);

    setFixedSize(_sceneWidth, _sceneHeight);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setRenderHint(QPainter::Antialiasing);
    setWindowTitle("AutoChess");
    setScene(scene);

}

void Game::drawFields()
{

    board = new Board();
    board->drawBoard();

    damageChart = new DamageChart();
    damageChart->drawDamageChart();

    bench = new Bench();
    bench->drawBench();

    shop = new Shop();
    shop->drawShop();

    stats = new Stats();
    stats->drawStats();

    controls = new Controls();
    controls->drawControls();

    timer = new Timer(nullptr, 25, 30);
    timer->drawTimer();

    economy = new Economy(nullptr);
    economy->drawEconomy();

}

void Game::mainMenu()
{
    QGraphicsTextItem *titleText = new QGraphicsTextItem("AutoChess");
    QFont titleFont("arial" , 50);
    titleText->setFont( titleFont);
    qint32 xPos = _sceneWidth/2 - titleText->boundingRect().width()/2 - 100;
    qint32 yPos = 150;
    titleText->setPos(xPos,yPos);
    addToScene(titleText);
    itemList.append(titleText);

    Button *playButton = new Button("Play");
    qint32 pxPos = _sceneWidth/2 - playButton->boundingRect().width()/2-100;
    qint32 pyPos = 300;
    playButton->setPos(pxPos,pyPos);
    connect(playButton,SIGNAL(clicked()) , this , SLOT(start()));
    addToScene(playButton);
    itemList.append(playButton);

    hardButton = new Button("Hard mode: Off");
    qint32 hxPos = width()/2 - playButton->boundingRect().width()/2-100;
    qint32 hyPos = 375;
    hardButton->setPos(hxPos,hyPos);
    connect(hardButton,SIGNAL(clicked()) , this , SLOT(hardMode()));
    addToScene(hardButton);
    itemList.append(hardButton);

    Button *quitButton = new Button("Quit");
    qint32 qxPos = width()/2 - quitButton->boundingRect().width()/2-100;
    qint32 qyPos = 450;
    quitButton->setPos(qxPos,qyPos);
    connect(quitButton, SIGNAL(clicked()),this,SLOT(close()));
    addToScene(quitButton);
    itemList.append(quitButton);

}

void Game::start()
{

    for(qint32 i =0, n = itemList.size(); i < n; i++) {
        removeFromScene(itemList[i]);
        delete itemList[i];
    }

    drawFields();

    MouseClickEater* mouseClickEater = new MouseClickEater(this);
    installEventFilter(mouseClickEater);

    unitManager = new UnitManager();

    shopPhase();

//    QMediaPlayer* music = new QMediaPlayer;
//    QAudioOutput* audioOutput = new QAudioOutput;
//    music->setAudioOutput(audioOutput);
//    audioOutput->setVolume(50);

//    QFileInfo* musicInfo = new QFileInfo(":/resources/sounds/autochessBackground.wav");
//    QString path = musicInfo->canonicalFilePath();
//    std::cout << path.toStdString() << std::endl;
//    music->setSource(QUrl::fromLocalFile(path));
//    //music->setSource(QUrl::fromLocalFile("/home/neop/Desktop/08-AutoChess/AutoChess/resources/sounds/autochessBackground.wav"));
//    music->setLoops(QMediaPlayer::Infinite);
//    //volume
//    music->play();
}
void Game::shopPhase()
{
    setPhase(Phase::shop);
    timer->updateTimerColor(_phase);
    economy->round++;
    if(economy->round == _maxRounds + 1){
        gameOver();
        return;
    }
    economy->updateRoundNum();
    economy->gold += 10;
    economy->updateGold();
    unitManager->increaseMaxUnitsOnBoard();
    if(!_shopLocked)
        shop->rollShopUnits();

    damageChart->deleteEnemyDamageBars();
    unitManager->rebuildBoard();
    unitManager->buildEnemyRoster(economy->round);

    timer->resetTime(_phase);
    timer->shopGuiTimer->start();
}

void Game::combatPhase()
{
    setPhase(Phase::combat);
    damageChart->resetUnitDamageDealt();
    if(_hardMode == HardMode::ON)
        board->showEnemyUnits();
    timer->updateTimerColor(_phase);
    timer->resetTime(_phase);
    timer->combatGuiTimer->start();
    timer->combatTick->start();
}

void Game::serializeRoundInfo()
{
    Result result = unitManager->showRoundWinner();
    _roundsWon = result==Result::win ? _roundsWon + 1 : _roundsWon ;
    _roundsEnemyWon = result==Result::loss ? _roundsEnemyWon + 1 : _roundsEnemyWon;

    QJsonObject round;
    QJsonArray unitsArray;
    round.insert("num", economy->round);
    round.insert("result", result==Result::win ? "win" : "loss");
    for(auto unit : board->bottomUnits)
    {
        unitsArray.append(unit->saveUnit());

    }
    round.insert("units", unitsArray);

    roundArray.append(round);
    gameJsonObject.insert("rounds", roundArray);
}

void Game::gameOver()
{
    QMessageBox msgBox;
    QString msgText;

    if(_roundsWon > _roundsEnemyWon)
        msgText = "YOU EMERGE VICTORIOUS!!!";
    else if(_roundsWon < _roundsEnemyWon)
        msgText = "YOU ARE DEFEATED!";
    else
        msgText = "STALEMATE!";

    msgText.append(" | You won: " + QString::number(_roundsWon) + " rounds! | Enemy won: " + QString::number(_roundsEnemyWon) + " rounds!");
    msgBox.setText(msgText);
    gameJsonObject.insert("game summary", msgText);
    QJsonDocument doc = QJsonDocument(gameJsonObject);
    QFile file(_savePath);

    file.open(QFile::WriteOnly | QFile::Text);
    file.write(doc.toJson(QJsonDocument::JsonFormat::Indented));
    file.close();

    delete unitManager;

    QPushButton *quitButton = msgBox.addButton(tr("Quit"), QMessageBox::ActionRole);
    connect(quitButton,&QPushButton::clicked,this,[&]{ delete this;});
    msgBox.exec();
}

void Game::hardMode()
{
    if(!_hardMode){
        hardButton->setTextTo("Hard mode: On");
        setGameMode(HardMode::ON);
    }else{
        hardButton->setTextTo("Hard mode: Off");
        setGameMode(HardMode::OFF);
    }
}

void Game::addToScene(QGraphicsItem *item)
{
    scene->addItem(item);
}

void Game::removeFromScene(QGraphicsItem *item)
{
    scene->removeItem(item);

}

QGraphicsProxyWidget* Game::addWidget(QWidget *widget)
{
    return scene->addWidget(widget);
}


Phase Game::getPhase()
{
    return _phase;
}

void Game::setPhase(Phase phase)
{
    _phase = phase;
}

HardMode Game::getGameMode()
{
    return _hardMode;
}

void Game::setGameMode(HardMode mode)
{
     _hardMode = mode;
}
