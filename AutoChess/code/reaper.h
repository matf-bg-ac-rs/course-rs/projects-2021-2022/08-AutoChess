#ifndef REAPER_H
#define REAPER_H

#include "unit.h"
#include "utils.h"

class Reaper : public Unit
{
public:
    Reaper(Team team, quint32 level, QGraphicsItem *parent = 0);

    void drawUnit() override;
    void setHighlight(bool hl);
    bool getHighlight();
    void acceptVisit(UnitVisitor& visitor) override {visitor.visitReaper(*this);};

    void findTarget() override;
    void getInRange() override;

    void castSpell() override;
    void attack() override;

    void giveStatsForLevel() override;

private:
    bool _highlight;
    float _bonusDmg;
};

#endif // REAPER_H
