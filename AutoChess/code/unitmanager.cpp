#include "unitmanager.h"
#include "game.h"
#include "algorithm"
#include "dreadherald.h"
#include "utils.h"
#include "unit.h"
#include "cell.h"
#include <QDebug>
#include <QMessageBox>

UnitManager::UnitManager()
{
    Game::game()->economy->maxunits = 1;
    Game::game()->economy->units = 0;
    Game::game()->economy->updateUnitsNum();
}

void UnitManager::openJSON(QString path)
{
    if(!doc.isNull())
        return;

    QFile inFile(path);
    inFile.open(QIODevice::ReadOnly);
    QByteArray data = inFile.readAll();
    inFile.close();

    QJsonParseError errorPtr;
    doc = QJsonDocument::fromJson(data, &errorPtr);
    if (doc.isNull())
    {
        qDebug() << "Parse failed";
    }
}



void UnitManager::buildEnemyRoster(qint32 roundNum)
{
    openJSON();
    QJsonObject rootObj = doc.object();
    QJsonArray roundsArray = rootObj.value("rounds").toArray();

    for(const QJsonValue& round :  roundsArray)
    {
//        qDebug() << " 1.";
        QJsonObject roundObject = round.toObject();
        if(roundObject.value("num").toInt() == roundNum)
        {
            QJsonArray unitArray = roundObject.value("units").toArray();
            for(const QJsonValue& unit :  unitArray)
            {
//                qDebug() << " 2.";
                QString type = unit.toObject().value("name").toString();
                quint32 x = unit.toObject().value("posx").toInt();
                quint32 y = unit.toObject().value("posy").toInt();
                quint32 level = unit.toObject().value("level").toInt();
                UnitType unitType = Unit::toUnitType(type);
                Unit* enemy = Unit::create(unitType, Team::top, level);
                enemy->drawUnit();
                Game::game()->unitManager->placeUnit(enemy, Game::game()->board->cells[x][y]);
//                qDebug() << " 3.";
            }
        }
    }
}

void UnitManager::rebuildBoard()
{
    for(auto unit : Game::game()->board->bottomUnits)
    {
        unit->resetUnit();
    }
    for(auto unit : Game::game()->board->topUnits)
    {
        delete unit;
    }
    Game::game()->board->topUnits.clear();
}

bool UnitManager::checkRoundEnd()
{
    bool bottomAlive = false;
    bool topAlive = false;
    for(auto unit : Game::game()->board->bottomUnits)
        if(!unit->_dead)
        {
            bottomAlive = true;
            break;
        }
    for(auto unit : Game::game()->board->topUnits)
        if(!unit->_dead)
        {
            topAlive = true;
            break;
        }

    return !bottomAlive || !topAlive;
}

void UnitManager::placeUnit(Unit* unit, Cell* cell)
{
    if (cell->getLocation() == Location::board) {

        if (unit->getParentCell() != nullptr)
            unit->getParentCell()->setTaken(false);


        unit->setParentCell(cell);
        unit->getParentCell()->setTaken(true);

        Node node = Node(unit->getParentCell()->_row, unit->getParentCell()->_col);
        unit->setNode(node);

        unit->hpBar->setVisible(true);
        if(unit->_maxMana != 0)
            unit->manaBar->setVisible(true);
        else
            unit->manaBar->setVisible(false);
        unit->hpBar->update();
        unit->manaBar->update();
        unit->particleManager->updateDisableParticles(unit->pos());

        Game::game()->board->addToBoard(unit);

        unit->setHighlight(false);
        unit->setImage();
        unit->update();

    }
    unit->levelIndicator->refresh();
    unit->update();
}

bool UnitManager::addToBoardCount()
{
    if(Game::game()->economy->maxunits <= Game::game()->economy->units)
        return false;
    Game::game()->economy->units++;
    Game::game()->economy->updateUnitsNum();
    return true;
}

void UnitManager::increaseMaxUnitsOnBoard()
{
    Game::game()->economy->maxunits += 2;
    Game::game()->economy->updateUnitsNum();
}

Result UnitManager::showRoundWinner()
{
    quint32 bottomLevelSum = 0;
    quint32 topLevelSum = 0;
    Result res;
    for(auto unit : Game::game()->board->bottomUnits)
    {
        if(!unit->_dead)
            bottomLevelSum += unit->_level;
    }
    for(auto unit : Game::game()->board->topUnits)
    {
        if(!unit->_dead)
            topLevelSum += unit->_level;
    }

    if(bottomLevelSum > topLevelSum)
        res = Result::win;
    else if(topLevelSum > bottomLevelSum)
        res = Result::loss;
    else
        res = Result::draw;

    roundResult(res);

    return res;
}

void UnitManager::roundResult(Result res)
{
    QMessageBox msgBox;
    if(res == Result::win)
    {
        msgBox.setText("YOU WON!");
    }
    else if(res == Result::loss)
        msgBox.setText("YOU LOST!");
    else
        msgBox.setText("DRAW!");
    msgBox.exec();
}
