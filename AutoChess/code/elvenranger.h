#ifndef ELVENRANGER_H
#define ELVENRANGER_H

#include "unit.h"
#include "utils.h"

class ElvenRanger : public Unit
{
public:
    ElvenRanger(Team team, quint32 level, QGraphicsItem *parent = 0);

    void drawUnit() override;
    void setHighlight(bool hl);
    bool getHighlight();
    void acceptVisit(UnitVisitor& visitor) override {visitor.visitElvenRanger(*this);};

    void castSpell() override;
    void attack() override;

    void giveStatsForLevel() override;

private:
    bool _highlight;
};

#endif // HORSEMAN_H
