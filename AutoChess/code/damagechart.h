#ifndef DAMAGECHART_H
#define DAMAGECHART_H

#include "utils.h"
#include <QWidget>

namespace Ui {
class DamageChart;
}

class DamageChart : public QWidget
{
    Q_OBJECT

public:
    explicit DamageChart(QWidget *parent = nullptr);
    ~DamageChart();

    QGraphicsProxyWidget *proxyParent;

    void drawDamageChart();

    void updateDamageBarPositions();
    void updateDamageChart(Team team, qint32 damage);
    void resetUnitDamageDealt();
    void deleteEnemyDamageBars();
private:
    Ui::DamageChart *ui;
};

#endif // DAMAGECHART_H
