#ifndef BENCH_H
#define BENCH_H

#include <QGraphicsObject>
#include <QPainter>
#include <QGraphicsScene>
#include "cell.h"
#include "unit.h"

class Bench : public QGraphicsObject

{
public:
    Bench();

    Cell* benchCells[8];

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;

    void drawBench();


private:
    qint32 _size;
    qint32 _x, _y;
    QColor _color;

};

#endif // BENCH_H
