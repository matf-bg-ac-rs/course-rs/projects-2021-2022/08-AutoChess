#include "necromage.h"
#include "game.h"


Necromage::Necromage(Team team, quint32 level, QGraphicsItem *parent)
    : Unit(team, level, UnitType::necromage, parent), _highlight(false)
{
    _hlPixmap = QPixmap(":/resources/images/necroMageHL.png");
    _originalPixmap = QPixmap(":/resources/images/necroMage.png");
    setImage();
    loadStats("Necromage");
    hpBar = new HpBar(this, _team, _maxHp, _hp);
    manaBar = new ManaBar(this, _team, _maxMana, _mana);
    giveStatsForLevel();
    Game::game()->addToScene(hpBar);
    Game::game()->addToScene(manaBar);
    hpBar->setVisible(false);
    manaBar->setVisible(false);

}

void Necromage::drawUnit()
{
    setZValue(ZValues::zUnit);
    Game::game()->addToScene(this);
    if(_team == Team::top && Game::game()->getGameMode() == HardMode::ON)
        hideUnit();
}

void Necromage::castSpell()
{
    if(!hasTarget() || _silenced)
        return;
    _mana = 0;
    manaBar->setMana(0);

    bool killed = false;
    particleManager->receiveParticle(ParticleType::spell, _type, pos());
    _target->particleManager->receiveParticle(ParticleType::received, _type, _target->pos());
    _target->particleManager->receiveParticle(ParticleType::blood, _target->_type, _target->pos());
    Unit* unit = findLowestHpAlly();
    unit->particleManager->receiveParticle(ParticleType::heal, unit->_type, unit->pos());
    unit->hpBar->setHp(unit->_hp + _spellDamage);
    unit->_hp = unit->hpBar->hp();

    killed = _target->takeDamage(_spellDamage);
    damageBar->addDamageDealt(_damage);

    if (killed) {
        _target->particleManager->clearParticles();
        _target->particleManager->clearDisableParticles();
        _target = nullptr;
    }

}

Unit* Necromage::findLowestHpAlly()
{
    qint32 minHp = Limits::infHp;
    Unit* unitToHeal;
    auto& units = _team == Team::bottom ? Game::game()->board->bottomUnits : Game::game()->board->topUnits;

    for(Unit* unit : units)
    {
        if(unit->_dead)
            continue;

        if(unit->_hp < minHp)
        {
            unitToHeal = unit;
            minHp = unit->_hp;
        }
    }

    return unitToHeal;
}

void Necromage::giveStatsForLevel()
{
    _maxHp = _hpPerLevel * (_level + 1);
    _hp = _maxHp;
    hpBar->setMaxHp(_maxHp);

    _originalSpellDamage = _spellDamagePerLevel * (_level + 1);
    _spellDamage = _originalSpellDamage;

    updateSpellText();
    _damage = _dmgPerLevel * (_level + 1);
}

void Necromage::updateSpellText()
{
    _spellText = "Necromage rips his enemy life force for " + QString::number(_spellDamage) + " damage and transfers it to ally with lowest hp";
}
