#include "horseman.h"
#include "game.h"

Horseman::Horseman(Team team, quint32 level, QGraphicsItem *parent)
    : Unit(team, level, UnitType::horseman, parent), _highlight(false)
{

    _originalPixmap = QPixmap(":/resources/images/horseman.png");
    _hlPixmap = QPixmap(":/resources/images/horsemanHL.png");
    setImage();
    _dmgReduction = 0;
    _stunTime = 0;

    loadStats("Horseman");
    hpBar = new HpBar(this, _team, _maxHp, _hp);
    manaBar = new ManaBar(this, _team, _maxMana, _mana);
    giveStatsForLevel();
    Game::game()->addToScene(hpBar);
    Game::game()->addToScene(manaBar);
    hpBar->setVisible(false);
    manaBar->setVisible(false);
}

void Horseman::drawUnit()
{
    setZValue(ZValues::zUnit);
    Game::game()->addToScene(this);
    if(_team == Team::top && Game::game()->getGameMode() == HardMode::ON)
        hideUnit();
}
bool Horseman::takeDamage(qint32 dmg)
{
    dmg -= dmg * _dmgReduction;
    _hp -= dmg;
    hpBar->setHp(_hp);

    if(_hp <= 0 && !_dead)
    {
        _dead = true;
        if(_highlight)
            deselectUnit();
        getParentCell()->setTaken(false);
        setVisible(false);
        _node = NotFound;
        hpBar->setVisible(false);
        manaBar->setVisible(false);
        levelIndicator->setVisible(false);

        return true;
    }

    return false;
}

void Horseman::attack()
{
    if (_target != nullptr && _target->_dead)
        return;

    if(isInRange() && remainingAttackCooldown <= 0) {
        remainingAttackCooldown = attackCooldown;
    } else {
        return;
    }

    particleManager->receiveParticle(ParticleType::attack, _type, pos());
    _target->particleManager->receiveParticle(ParticleType::received, _type, _target->pos());
    _target->particleManager->receiveParticle(ParticleType::blood, _target->_type, _target->pos());
    bool killed = false;

    killed = _target->takeDamage(_damage);
    damageBar->addDamageDealt(_damage);
    if(_hitCount == 3)
        stunTarget();
    _hitCount = _hitCount < 3 ? _hitCount + 1 : 0;

    if (killed) {
        _target->particleManager->clearParticles();
        _target->particleManager->clearDisableParticles();
        _target = nullptr;
    }

}

void Horseman::stunTarget()
{
    _target->_stunned = true;

    _target->particleManager->receiveParticle(ParticleType::disable, _type, _target->pos());

    stunDuration = new QTimer(this);
    stunDuration->setInterval(1000 * _stunTime);

    stunDuration->start();
    connect(stunDuration, &QTimer::timeout, this, [&]
    {
        if(_target != nullptr){
            _target->_stunned = false;
            _target->particleManager->clearDisableParticles();
        }

        stunDuration->stop();
    });
}

void Horseman::castSpell()
{
    return;
}

void Horseman::giveStatsForLevel()
{
    _maxHp = _hpPerLevel * (_level + 1);
    _hp = _maxHp;
    hpBar->setMaxHp(_maxHp);
    _dmgReduction = 0.075 * (_level + 1);
    updateSpellText();
    _damage = _dmgPerLevel * (_level + 1);
    _stunTime = 0.6 * (_level + 1);
    _hitCount = 0;
}

void Horseman::updateSpellText()
{
    _spellText = "Horseman takes " + QString::number(_dmgReduction, 'f', 3) + "% less damage. Every 4th Horsemans` attack stuns his target for " + QString::number(_stunTime, 'f', 1) + " seconds ";
}

