#include "damagechart.h"
#include "ui_damagechart.h"
#include "game.h"
#include <QDebug>


DamageChart::DamageChart(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DamageChart)
{
    ui->setupUi(this);
    move(400+8*96 + 20,-85+120);
}

DamageChart::~DamageChart()
{
    delete ui;
}

void DamageChart::drawDamageChart()
{
    proxyParent = Game::game()->addWidget(this);
    resetUnitDamageDealt();
}

void DamageChart::updateDamageBarPositions()
{
    quint32 i = 0;
    for(auto unit : Game::game()->board->bottomUnits)
    {
        unit->damageBar->setPos(pos().x() + 120 + DamageBarSize::gap , pos().y() + 80 + DamageBarSize::height * i + DamageBarSize::gap * i);
        unit->damageBar->setTextPos();
        i++;
    }

    ui->topTeam->setGeometry(ui->topTeam->pos().x(), pos().y() + 45 + DamageBarSize::height * i + DamageBarSize::gap * i, DamageBarSize::width, DamageBarSize::height);
    i++;

    for(auto unit : Game::game()->board->topUnits)
    {
        unit->damageBar->moveBy(pos().x() + 120 + DamageBarSize::gap , pos().y() + 80 + DamageBarSize::height * i + DamageBarSize::gap * i);
        unit->damageBar->setTextPos();
        i++;
    }
}

void DamageChart::updateDamageChart(Team team, qint32 damage)
{
    if(team == Team::bottom)
    {
        for(auto unit : Game::game()->board->bottomUnits)
        {
            unit->damageBar->setMaxDamageDealt(damage);
            unit->damageBar->update();
        }
    }
    else if(team == Team::top)
    {
        for(auto unit : Game::game()->board->topUnits)
        {
            unit->damageBar->setMaxDamageDealt(damage);
            unit->damageBar->update();
        }
    }

}

void DamageChart::resetUnitDamageDealt()
{
    for(auto unit : Game::game()->board->bottomUnits)
    {
        unit->damageBar->setVisible(true);
        unit->damageBar->resetDamageDealt();
        unit->damageBar->update();
    }
    for(auto unit : Game::game()->board->topUnits)
    {
        unit->damageBar->setVisible(true);
        unit->damageBar->resetDamageDealt();
        unit->damageBar->update();
    }

    updateDamageBarPositions();
}

void DamageChart::deleteEnemyDamageBars()
{
    for(auto unit : Game::game()->board->topUnits)
    {
        unit->damageBar->setVisible(false);
        delete unit->damageBar;
        unit->damageBar = nullptr;
    }
}


