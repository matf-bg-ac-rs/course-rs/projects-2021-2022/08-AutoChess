#ifndef BOARD_H
#define BOARD_H

#include <QGraphicsObject>
#include <QPainter>
#include <QGraphicsScene>
#include "cell.h"
#include "unit.h"
#include "graph.h"
#include "utils.h"

class Board : public QGraphicsObject
{
public:
    Board();

    Cell* cells[8][8];
    QVector<Unit*> topUnits;
    QVector<Unit*> bottomUnits;
    Graph* graph;

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;

    void drawBoard();
    void removeFromBoard(Unit* unit);
    void addToBoard(Unit* selectedUnit);
    void showEnemyUnits();
public slots:
    void timerEndmoveNorth();

private:
    QColor color;
    QColor originalColor;
    qint32 _x, _y;
};

#endif // BOARD_H
