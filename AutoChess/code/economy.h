#ifndef ECONOMY_H
#define ECONOMY_H

#include <QWidget>
#include <QGraphicsProxyWidget>

namespace Ui {
class Economy;
}

class Economy : public QWidget
{
    Q_OBJECT

public:
    explicit Economy(QWidget *parent = nullptr, unsigned round = 0, unsigned gold = 0);
    ~Economy();
    QGraphicsProxyWidget *proxyParent;
    void drawEconomy();
    void updateRoundNum();
    void updateGold();
    void updateUnitsNum();

    qint32 round;
    qint32 gold;
    qint32 units;
    qint32 maxunits;

    Ui::Economy *ui;
};

#endif // ECONOMY_H
