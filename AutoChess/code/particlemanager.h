#ifndef PARTICLEMANAGER_H
#define PARTICLEMANAGER_H


#include <vector>
#include "particle.h"
#include <QPoint>

class ParticleManager
{
public:
    ParticleManager();

    std::vector<Particle*> receivedParticles;
    std::vector<QPoint> receivedOffsets;

    Particle* attackParticle;
    QPoint attackOffset;

    Particle* bloodParticle;
    QPoint bloodOffset;

    Particle* disableParticle;
    QPoint disableOffset;

    void receiveParticle(ParticleType particleType, UnitType unitType, QPointF unitPos);

    void clearParticles();
    void clearDisableParticles();

    void updateDisableParticles(QPointF unitPos);

private:


};

#endif // PARTICLEMANAGER_H
