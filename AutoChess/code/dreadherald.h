#ifndef DREADHERALD_H
#define DREADHERALD_H

#include <QTimer>
#include "unit.h"
#include "utils.h"

class DreadHerald : public Unit
{
public:
    DreadHerald(Team team, quint32 level, QGraphicsItem *parent = 0);

    QTimer *duration;
    void drawUnit() override;
    void setHighlight(bool hl);
    bool getHighlight();
    void acceptVisit(UnitVisitor& visitor) override {visitor.visitDreadHerald(*this);};

    void castSpell() override;

    void giveStatsForLevel() override;

private:
    float _spellDuration;
    bool _highlight;
};

#endif // DREADHERALD_H
