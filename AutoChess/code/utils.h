#ifndef UTILS_H
#define UTILS_H

#include <QString>

class ZValues
{
public:
    static const qint32 zBoard = 40;
    static const qint32 zCell = 50;
    static const qint32 zUnit = 100;
    static const qint32 zBloodParticle = 119;
    static const qint32 zDisableParticle = 118;
    static const qint32 zParticle = 120;
    static const qint32 zLevelIndicator = 130;
    static const qint32 zHpBar = 200;
    static const qint32 zDamageBar = 200;
    static const qint32 zDamageText = 201;
};
enum class ParticleType
{
    received,
    attack,
    blood,
    spell,
    disable,
    heal
};

enum class Result
{
    win,
    loss,
    draw
};

enum class Location
{
    shop,
    bench,
    board,
    unused
};

enum class UnitType
{
    sunknight,
    reaper,
    necromage,
    dreadherald,
    philosopher,
    elvenranger,
    horseman
};

enum Team
{
    bottom = 1389,
    top
};

enum BuyOrNoBuy
{
    buy,
    nobuy
};

enum HardMode
{
    OFF,
    ON
};

enum Phase
{
    shop,
    combat
};

class Limits
{
public:
  static const qint32 infDistance = 1000;
  static const qint32 infHp = 1000000;
};

class Ticks
{
public:
    static const qint32 oneSecond = 1000;
    static const qint32 minTickInterval = 200;
};

class DamageBarSize
{
public:
    static const qint32 width = 180;
    static const qint32 height = 20;
    static const qint32 gap = 10;
    static const qint32 x = 1208;
    static const qint32 y = 150;
};

#endif // UTILS_H
