#include <QDrag>
#include <QMimeData>
#include <QGraphicsItem>
#include <QCursor>
#include <QApplication>
#include <QWidget>
#include <iostream>
#include <QKeyEvent>

#include "unit.h"
#include "game.h"
#include "unitmanager.h"

#include <queue>
#include <vector>
#include <map>
#include <QRandomGenerator>


extern Unit* selectedUnit;
extern Cell* selectedCell;

Unit::Unit(Team team, quint32 level, UnitType type, QGraphicsItem *parent)
    : QGraphicsObject(parent), _team(team), _type(type), _level(level)
{
    setFlags(GraphicsItemFlag::ItemIsSelectable);
    _parentCell = nullptr;
    setHighlight(false);
    _casted = false;
    _silenced = false;
    _stunned = false;
    _cost = 1;

    remainingMoveCooldown = moveCooldown = 500;
    remainingMoveCooldown += QRandomGenerator::global()->bounded(1001);

    _destination = NotFound;
    _dead = false;
    _target = nullptr;

    particleManager = new ParticleManager();

    levelIndicator = new LevelIndicator(_level, this);
    Game::game()->addToScene(levelIndicator);
    levelIndicator->refresh();

    damageBar = new DamageBar(nullptr, _team, toString(_type), " " + QString::number(_level));

    setFlags(QGraphicsItem::ItemIsFocusable);
}

Unit *Unit::create(UnitType type, Team team, quint32 level)
{
    switch(type){
        case UnitType::sunknight:
            return new Knight(team, level);
        case UnitType::reaper:
            return new Reaper(team, level);
        case UnitType::necromage:
            return new Necromage(team, level);
        case UnitType::dreadherald:
            return new DreadHerald(team, level);
        case UnitType::philosopher:
            return new Philosopher(team, level);
        case UnitType::elvenranger:
            return new ElvenRanger(team, level);
        case UnitType::horseman:
            return new Horseman(team, level);
        default:
            return new Knight(team, level);
    }
}

UnitType Unit::toUnitType(QString type)
{
        if(type == "Knight")
            return UnitType::sunknight;
        else if(type == "Reaper")
            return UnitType::reaper;
        else if(type == "Necromage")
            return UnitType::necromage;
        else if(type == "DreadHerald")
            return UnitType::dreadherald;
        else if(type == "Philosopher")
            return UnitType::philosopher;
        else if(type == "ElvenRanger")
            return UnitType::elvenranger;
        else if(type == "Horseman")
            return UnitType::horseman;

        return UnitType::sunknight;
}

QString Unit::toString(UnitType type)
    {
        switch(type)
        {
            case UnitType::sunknight:
                return "Sunknight";
            case UnitType::reaper:
                return "Reaper";
            case UnitType::necromage:
                return "Necromage";
            case UnitType::dreadherald:
                return "DreadHerald";
            case UnitType::philosopher:
                return "Philosopher";
            case UnitType::elvenranger:
                return "ElvenRanger";
            case UnitType::horseman:
                return "Horseman";
        }
    }

Cell *Unit::getOriginalCell()
{
    return _originalCell;
}

void Unit::setOriginalCell(Cell *newOriginalCell)
{
    _originalCell = newOriginalCell ;
}

Cell *Unit::getParentCell()
{
    return _parentCell;
}

void Unit::setParentCell(Cell* cell)
{
    _parentCell = cell;
    setPos(cell->pos() + QPoint(3, 3));
    hpBar->setPos(cell->pos() + QPoint(4, -6));
    manaBar->setPos(cell->pos() + QPoint(4, 0));

    levelIndicator->refresh();

    update();
    cell->update();
}

void Unit::resetUnit()
{
    _hp = _maxHp;
    hpBar->setHp(_hp);

    _mana = 0;
    manaBar->setMana(0);

    attackCooldown = originalAttackCooldown;
    _spellDamage = _originalSpellDamage;
    _silenced = false;
    _stunned = false;
    _casted = false;
    _dead = false;
    _target = nullptr;
    getParentCell()->setTaken(false);
    setVisible(true);
    hpBar->setVisible(true);
    if(_maxMana != 0)
        manaBar->setVisible(true);
    levelIndicator->setVisible(false);
    remainingMoveCooldown = moveCooldown;
    if(_type == UnitType::reaper)
        remainingMoveCooldown = 2000;
    remainingAttackCooldown = attackCooldown;
    Game::game()->unitManager->placeUnit(this, _originalCell);
    _node = {_parentCell->_row, _parentCell->_col};

    particleManager->clearParticles();
    particleManager->clearDisableParticles();

}

QRectF Unit::boundingRect() const
{
    return QRectF(0, 0, 96, 96);
}

void Unit::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    Q_UNUSED(option)
    painter->drawPixmap(0, 0, 90, 90, _scaledPixmap);
}

void Unit::setImage()
{
    if(_highlight)
        _pixmap = _hlPixmap;
    else
        _pixmap = _originalPixmap;

    _scaledPixmap = QPixmap(_pixmap.scaled(90, 90, Qt::KeepAspectRatio));
}

void Unit::openJSON(QString path)
{
    if(!doc.isNull())
        return;

    QFile inFile(path);
    inFile.open(QIODevice::ReadOnly);
    QByteArray data = inFile.readAll();
    inFile.close();

    QJsonParseError errorPtr;
    doc = QJsonDocument::fromJson(data, &errorPtr);
    if (doc.isNull())
    {
        qDebug() << "Parse failed";
    }
}

void Unit::loadStats(QString type)
{
    openJSON();
    QJsonObject rootObj = doc.object();
    QJsonArray unitsArray = rootObj.value("units").toArray();

    for(const QJsonValue& unit :  unitsArray)
    {
        QJsonObject unitObject = unit.toObject();
        if(unitObject.value("name")==type)
        {
            _hpPerLevel             = unitObject.value("hpPerLevel").toInt();
            _maxHp = _hp            = _hpPerLevel * 2;
            _maxMana                = unitObject.value("maxMana").toInt();
            _mana                   = unitObject.value("mana").toInt();
            _dmgPerLevel            = unitObject.value("dmgPerLevel").toInt();
            _spellDamagePerLevel    = unitObject.value("spellDamage").toInt();
            _range                  = unitObject.value("range").toInt();
            _manaPerHit             = unitObject.value("manaPerHit").toInt();
            originalAttackCooldown  = unitObject.value("cooldown").toInt();
            attackCooldown          = originalAttackCooldown;
            remainingAttackCooldown = originalAttackCooldown;
            _spellName              = unitObject.value("spellName").toString();
        }
    }
}

QJsonObject Unit::saveUnit()
{
    QJsonObject unit;
    unit.insert("name", toString(_type));
    unit.insert("posx", getParentCell()->_col);
    unit.insert("posy", getParentCell()->_row);
    unit.insert("level", _level);
    return unit;
}

void Unit::deselectUnit()
{
    selectedUnit->setHighlight(false);
    selectedUnit->setImage();
    selectedUnit->update();
    selectedUnit = nullptr;
    clearFocus();
    connect(this, &Unit::unitIsNOTHighlighted, Game::game()->stats,&Stats::clearStats);
    emit unitIsNOTHighlighted();
}

void Unit::moveToBench()
{
    for(auto cell : Game::game()->bench->benchCells)
    {
        if(!cell->isTaken())
        {
            if(getParentCell()->getLocation() == Location::board)
                Game::game()->board->removeFromBoard(this);
            if (getParentCell()->getLocation() == Location::bench)
                getParentCell()->setTaken(false);
            setParentCell(cell);
            getParentCell()->setTaken(true);
            getParentCell()->setLocation(Location::bench);
            setOriginalCell(cell);

            hpBar->setVisible(false);
            manaBar->setVisible(false);
            update();

            break;
        }
    }
}

void Unit::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event)
    if (getParentCell()->getLocation() == Location::shop)
    {
        if(selectedUnit != nullptr)
            deselectUnit();

        bool isFull = true;
        for (auto cell : Game::game()->bench->benchCells)
            if (!cell->isTaken())
                isFull = false;

        if (isFull)
            return;

        if (Game::game()->economy->gold < _cost) {
            Game::game()->shop->noMoneyForUnitMsg();
            qDebug("NOT ENOUGH GOLD");
            return;
        }

        Game::game()->economy->gold -= _cost;
        Game::game()->economy->updateGold();
        Game::game()->shop->removeFromShopUnits(this);

        moveToBench();
        Game::game()->shop->labelRefresh();
    }
    else
    {
        if (selectedUnit == nullptr)
        {
            selectedUnit = this;
            selectedUnit->setHighlight(true);
            selectedUnit->setImage();
            selectedUnit->update();

            for (auto i = 0; i < 8; i++)
                for (auto j = 0; j < 8 ; j++)
                {
                    Cell* cell = Game::game()->board->cells[i][j];
                    connect(cell, &Cell::CellClicked, this, &Unit::destCellClicked);
                    connect(cell, &Cell::CellClicked, Game::game()->stats, &Stats::clearStats);
                }

            connect(this, &Unit::unitIsHighlighted, Game::game()->stats,[]{ Game::game()->stats->fillStats(selectedUnit);});
            emit unitIsHighlighted();
        }
        else if(Game::game()->getPhase() == Phase::combat && selectedUnit->_team == Team::top && selectedUnit != this)
        {
            deselectUnit();
        }
        else if(Game::game()->getPhase() == Phase::shop && selectedUnit != this && selectedUnit->_type == _type && selectedUnit->_level == _level)
        {
            merge();
        }
        else if(selectedUnit == this)
        {
            deselectUnit();
        }
    }
}

void Unit::keyPressEvent(QKeyEvent *event)
{
    if(Game::game()->getPhase() == Phase::combat && selectedUnit->getParentCell()->getLocation() == Location::board)
    {
        deselectUnit();
        return;
    }
    if (selectedUnit == this)
    {
        if(event->key() == Qt::Key_W)
        {
            moveToBench();
            deselectUnit();
        }
        if(event->key() == Qt::Key_S)
        {
            Game::game()->economy->gold += ceil(_level/2.0);
            Game::game()->economy->updateGold();
            Game::game()->shop->unitPool->addToPool(getType());
            if(getParentCell()->getLocation() == Location::board)
                Game::game()->board->removeFromBoard(this);
            deselectUnit();

            delete this;
        }
    }
}

void Unit::destCellClicked()
{
    if (selectedUnit == nullptr)
    {
            return;
    }

    if(Game::game()->getPhase() == Phase::combat || selectedCell->_col < 4 || (selectedUnit->_parentCell->getLocation() != Location::board && !Game::game()->unitManager->addToBoardCount()))
    {
        deselectUnit();
        return;
    }

    Game::game()->unitManager->placeUnit(selectedUnit, selectedCell);
    selectedUnit->setOriginalCell(selectedCell);

    selectedUnit = nullptr;
}

void Unit::merge()
{
    if (_team == Team::top)
        return;

    _level++;
    damageBar->setLvl(QString::number(_level));
    giveStatsForLevel();

    levelIndicator->refresh();

    if(selectedUnit->getParentCell()->getLocation() == Location::board)
    {
        Game::game()->board->removeFromBoard(selectedUnit);
    }

    delete selectedUnit;
    selectedUnit = nullptr;

}

void Unit::hideUnit()
{
    setVisible(false);
    levelIndicator->setVisible(false);
    hpBar->setVisible(false);
    manaBar->setVisible(false);
}

void Unit::showUnit()
{
    setVisible(true);
    levelIndicator->setVisible(true);
    hpBar->setVisible(true);
    if(_maxMana != 0)
        manaBar->setVisible(true);
}

void Unit::setHighlight(bool hl)
{
    _highlight = hl;
}

bool Unit::getHighlight()
{
    return _highlight;
}
void Unit::setNode(Node node)
{
    _node = node;
}
Node Unit::getNode()
{
    return _node;
}

UnitType Unit::getType()
{
    return _type;
}
bool Unit::hasTarget()
{
    return _target != nullptr;
}

bool Unit::isInRange()
{
    return _target != nullptr && distance(_node, _target->_node) <= _range;
}

void Unit::findTarget()
{
    auto& graph = Game::game()->board->graph;

    qint32 minDistance = Limits::infDistance;

    if (_target != nullptr && _target->_dead)
        _target = nullptr;

    if (_target != nullptr && graph->astar(_node, _target->_node) != NotFound)
        minDistance = distance(_node, _target->_node);

    auto& units = _team == Team::bottom ? Game::game()->board->topUnits : Game::game()->board->bottomUnits;

    for(Unit* unit : units)
    {
        if(unit->_dead)
            continue;

        if(distance(_node, unit->_node) < minDistance && graph->astar(_node, unit->_node) != NotFound)
        {
            minDistance = distance(_node, unit->_node);
            _target = unit;
        }
    }

    if (minDistance == Limits::infDistance)
        _target = nullptr;

}

void Unit::moveStep(Node dest)
{
    Cell* destCell = Game::game()->board->cells[dest.first][dest.second];
    Game::game()->unitManager->placeUnit(this, destCell);
}

void Unit::getInRange()
{
    auto& graph = Game::game()->board->graph;

    if (remainingMoveCooldown > 0){
        return;
    } else {
        remainingMoveCooldown = moveCooldown;
    }

    if (_target != nullptr && _target->_dead)
        _target = nullptr;

    if (_target == nullptr)
        return;

    if (distance(_node, _target->_node) <= _range)
        return;

    auto neighbors = graph->getNeighbors(_target->_node);
    if (neighbors.size() == 0)
        return;

    Node minDistNeighbor = NotFound;

    for (auto neighbor : neighbors) {
        if (minDistNeighbor == NotFound) {
            minDistNeighbor = neighbor;
        } else {
            if (distance(_node, neighbor) < distance(_node, minDistNeighbor))
                minDistNeighbor = neighbor;
        }
    }

    Node nextNode = graph->astar(_node, minDistNeighbor);
    if (nextNode == NotFound)
        return;
    moveStep(nextNode);
}


bool Unit::takeDamage(qint32 dmg)
{
    _hp -= dmg;
    hpBar->setHp(_hp);

    if(_maxMana != 0)
    {
        manaBar->addMana(dmg/(6 + Game::game()->economy->round/2));
        _mana = manaBar->mana();
    }

    if(_hp <= 0 && !_dead)
    {
        _dead = true;
        if(_highlight)
            deselectUnit();
        getParentCell()->setTaken(false);
        setVisible(false);
        _node = NotFound;
        hpBar->setVisible(false);
        manaBar->setVisible(false);
        levelIndicator->setVisible(false);

        return true;
    }

    return false;
}

void Unit::updateCooldowns()
{
    remainingAttackCooldown -= Ticks::minTickInterval;
    remainingMoveCooldown -= Ticks::minTickInterval;
}

void Unit::attack()
{
    if (_target != nullptr && _target->_dead)
        return;

    if(isInRange() && remainingAttackCooldown <= 0) {
        remainingAttackCooldown = attackCooldown;
    } else {
        return;
    }

    particleManager->receiveParticle(ParticleType::attack, _type, pos());
    _target->particleManager->receiveParticle(ParticleType::received, _type, _target->pos());
    _target->particleManager->receiveParticle(ParticleType::blood, _target->_type, _target->pos());
    bool killed = false;

    killed = _target->takeDamage(_damage);
    damageBar->addDamageDealt(_damage);
    manaBar->addMana(_manaPerHit);
    _mana = manaBar->mana();

    if (killed) {
        _target->particleManager->clearParticles();
        _target->particleManager->clearDisableParticles();
        _target = nullptr;
    }

}

void Unit::executeNextMove()
{
    if (_dead)
        return;

    if(_highlight)
        Game::game()->stats->updateStats(this);

    updateCooldowns();
    findTarget();
    getInRange();
    if(!_stunned)
    {
        if(_mana == _maxMana)
            castSpell();
        attack();
    }

}

Unit::~Unit()
{
    delete damageBar;
    particleManager->clearParticles();
    particleManager->clearDisableParticles();
    delete particleManager;
    getParentCell()->setTaken(false);
    Game::game()->removeFromScene(hpBar);
    Game::game()->removeFromScene(manaBar);
    Game::game()->removeFromScene(levelIndicator);
    Game::game()->removeFromScene(this);
    delete manaBar;
    delete hpBar;
}

void Unit::updateSpellText()
{
    return;
}
