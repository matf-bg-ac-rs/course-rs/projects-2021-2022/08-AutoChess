#include "economy.h"
#include "ui_economy.h"
#include "game.h"

Economy::Economy(QWidget *parent, quint32 round, quint32 gold) :
    QWidget(parent),
    round(round),
    gold(gold),
    units(0),
    maxunits(0),
    ui(new Ui::Economy)
{
    ui->setupUi(this);
    move(400+8*96+120 + 30,-85 - 5);
}
void Economy::updateRoundNum()
{
    ui->lRoundNum->setText(QString::number(round));
}

void Economy::updateGold()
{
    ui->lGold->setText(QString::number(gold));
}

void Economy::updateUnitsNum()
{
    ui->lUnitsNum->setText(QString::number(units) + "/" + QString::number(maxunits));
}

void Economy::drawEconomy()
{
    proxyParent = Game::game()->addWidget(this);
    updateRoundNum();
    updateGold();
    updateUnitsNum();
}

Economy::~Economy()
{
    delete ui;
}
