#ifndef UICELL_H
#define UICELL_H

#include "cell.h"
#include <QString>
#include <QGraphicsPixmapItem>

class UICell : public Cell
{
public:
    UICell(QGraphicsObject* parent);
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;
    QPixmap _originalPixmap;
};

#endif // UICELL_H
