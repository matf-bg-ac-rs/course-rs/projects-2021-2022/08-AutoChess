#ifndef UNIT_H
#define UNIT_H

#include <QGraphicsObject>
#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include <QByteArray>
#include <QJsonParseError>
#include <QJsonDocument>
#include <QJsonValue>
#include <QDebug>

#include "utils.h"
#include "cell.h"
#include "hpbar.h"
#include "manabar.h"
#include "damagebar.h"
#include "graph.h"
#include "unitVisitor.h"
#include "particlemanager.h"
#include "levelindicator.h"

class Unit : public QGraphicsObject
{
    Q_OBJECT

public:
    Unit(Team team, quint32 level, UnitType type, QGraphicsItem *parent = 0);
    static Unit* create(UnitType type, Team team, quint32 level);
    static UnitType toUnitType(QString type);
    static QString toString(UnitType type);
    ~Unit();
    void setImage();
    virtual void acceptVisit(UnitVisitor& visitor)=0;
    virtual void drawUnit() = 0;

    void openJSON(QString path = ":/config/unitstats.json");
    QJsonDocument doc;
    void loadStats(QString type);
    QJsonObject saveUnit();

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;

    //pixmap
    QPixmap _originalPixmap;
    QPixmap _hlPixmap;
    QPixmap _pixmap;
    QPixmap _scaledPixmap;

    //cells
    Cell *getOriginalCell();
    void setOriginalCell(Cell *newOriginalCell);
    Cell *getParentCell();
    void setParentCell(Cell *newParentCell);
    void destCellClicked();

    void setHighlight(bool hl);
    bool getHighlight();

    qint32 _maxHp;
    qint32 _hp;
    qint32 _maxMana;
    qint32 _mana;
    qint32 _damage;
    qint32 _range;
    qint32 _manaPerHit;

    bool _dead;

    qint32 attackCooldown;
    qint32 originalAttackCooldown;
    qint32 remainingAttackCooldown;
    qint32 moveCooldown;
    qint32 remainingMoveCooldown;

    Node getNode();
    void setNode(Node node);

    UnitType getType();

    HpBar* hpBar;
    ManaBar* manaBar;
    DamageBar* damageBar;

    LevelIndicator* levelIndicator;

    ParticleManager* particleManager;

    Team _team;
    UnitType _type;
    qint32 _level;
    quint32 _hpPerLevel;
    quint32 _dmgPerLevel;
    quint32 _spellDamagePerLevel;

    bool _highlight;
    Cell *_originalCell;
    Cell* _parentCell;

    qint32 _cost;

    Unit* _target;
    Node _node;
    Node _destination;

    QString _spellName;
    QString _spellText;
    quint32 _spellDamage;
    quint32 _originalSpellDamage;
    bool _silenced;
    bool _stunned;
    bool _casted;

    bool hasTarget();
    bool isInRange();
    void moveStep(Node dest);

    virtual void findTarget();
    virtual void getInRange();
    virtual void attack();
    virtual bool takeDamage(qint32 dmg);
    virtual void castSpell() = 0;
    virtual void updateSpellText();
    virtual void giveStatsForLevel() = 0;

    void deselectUnit();
    void moveToBench();
    void updateCooldowns();
    void executeNextMove();
    void resetUnit();
    void merge();
    void hideUnit();
    void showUnit();

signals:
    void unitIsHighlighted();
    void unitIsNOTHighlighted();

public slots:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
};

#endif // UNIT_H
