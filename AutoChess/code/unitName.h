#ifndef UNITNAME_H
#define UNITNAME_H
#include "unitVisitor.h"
#include <QString>
#include "unit.h"

class UnitName : public UnitVisitor
{
public:
    UnitName(){}
    ~UnitName(){}
    UnitName(Unit& unit){ unit.acceptVisit(*this); }

    void visitKnight( Knight&  )                {name_ = "Knight";}
    void visitReaper( Reaper&  )                {name_ = "Reaper";}
    void visitNecromage( Necromage&  )          {name_ = "Necromage";}
    void visitDreadHerald( DreadHerald&  )      {name_ = "Dread Herald";}
    void visitPhilosopher( Philosopher&  )      {name_ = "Philosopher";}
    void visitElvenRanger( ElvenRanger&  )      {name_ = "Elven Ranger";}
    void visitHorseman( Horseman&  )            {name_ = "Horseman";}

    QString name_;
};

#endif // UNITNAME_H
