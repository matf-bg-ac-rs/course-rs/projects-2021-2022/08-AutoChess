#ifndef PHILOSOPHER_H
#define PHILOSOPHER_H

#include "unit.h"
#include "utils.h"
#include "unitVisitor.h"

class Philosopher : public Unit
{
public:
    Philosopher(Team team, quint32 level, QGraphicsItem *parent = 0);

    void drawUnit() override;
    void setHighlight(bool hl);
    bool getHighlight();
    void acceptVisit(UnitVisitor& visitor) override {visitor.visitPhilosopher(*this);};

    void castSpell() override;

    void giveStatsForLevel() override;

private:
    quint32 _spellBoost;
    bool _highlight;
};

#endif // PHILOSOPHER_H
