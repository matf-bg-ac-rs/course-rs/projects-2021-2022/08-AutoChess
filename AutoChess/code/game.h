#ifndef GAME_H
#define GAME_H

#include <QApplication>
#include <QtGui>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QColor>
#include <QWidget>
#include <QGraphicsProxyWidget>

#include "bench.h"
#include "board.h"
#include "button.h"
#include "cell.h"
#include "damagechart.h"
#include "economy.h"
#include "graph.h"
#include "shop.h"
#include "stats.h"
#include "timer.h"
#include "unit.h"
#include "unitmanager.h"
#include "controls.h"

#include "dreadherald.h"
#include "elvenranger.h"
#include "horseman.h"
#include "knight.h"
#include "necromage.h"
#include "philosopher.h"
#include "reaper.h"


class Game : public QGraphicsView
{
    Q_OBJECT
private:

    static Game* instance;
    Game(const Game& game) = delete;
    Game& operator=(const Game& game) = delete;

public:
    Game(QWidget* parent = 0);

    static int count;
    static Game* game();

    void drawFields();
    void mainMenu();

    void addToScene(QGraphicsItem* item);
    void removeFromScene(QGraphicsItem* item);

    QJsonObject gameJsonObject;
    QJsonArray roundArray;
    QJsonObject saveRound(Result result);

    void gameOver();


    QGraphicsProxyWidget* addWidget(QWidget *widget);

    Board* board;
    Bench* bench;
    DamageChart* damageChart;
    Shop* shop;
    Stats* stats;
    Controls* controls;
    Graph* graph;
    Button* hardButton;

    Timer* timer;
    Economy* economy;
    UnitManager* unitManager;

    Phase getPhase();
    void setPhase(Phase phase);
    HardMode getGameMode();
    void setGameMode(HardMode mode);

    void shopPhase();
    void combatPhase();

    void serializeRoundInfo();

public slots:
    void hardMode();
    void start();


private:
    qint32 _sceneWidth;
    qint32 _sceneHeight;
    HardMode _hardMode;
    Phase _phase;
    QGraphicsScene* scene;
    QList <QGraphicsItem*> itemList;
    bool _shopLocked;
    qint32 _maxRounds;
    qint32 _roundsWon;
    qint32 _roundsEnemyWon;
    QString _savePath;
};

#endif // GAME_H
