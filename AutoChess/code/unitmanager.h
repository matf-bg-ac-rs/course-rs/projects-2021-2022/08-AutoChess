#ifndef UNITMANAGER_H
#define UNITMANAGER_H

#include "unit.h"
#include "cell.h"

class UnitManager
{
public:
    UnitManager();

    void openJSON(QString path = ":/config/rounds.json");
    QJsonDocument doc;

    void buildEnemyRoster(qint32 roundNum);
    void rebuildBoard();
    void placeUnit(Unit* unit, Cell* cell);
    bool addToBoardCount();
    bool checkRoundEnd();
    void increaseMaxUnitsOnBoard();
    Result showRoundWinner();
    void roundResult(Result res);
};




#endif // UNITMANAGER_H
