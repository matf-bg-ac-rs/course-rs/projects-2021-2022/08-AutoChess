#include "manabar.h"
#include "game.h"
#include <QPainter>


ManaBar::ManaBar(QGraphicsObject* parent, Team team, qint32 maxMana, qint32 mana)
    : _team(team)
{
    if (team == Team::bottom) {
        _color = Qt::blue;
    } else {
        _color = Qt::darkBlue;
    }
    setPos(parent->pos());
    setZValue(ZValues::zHpBar);
    _maxMana = maxMana;
    _mana = mana;
}

QRectF ManaBar::boundingRect() const
{
    return QRectF(0, 0, 90, 6);
}
void ManaBar::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    Q_UNUSED(option)

    painter->setBrush(Qt::black);
    painter->drawRect(0, 0, 90, 6);
    painter->setBrush(_color);
    if(_maxMana != 0)
        painter->drawRect(0, 0, _mana * 90 / _maxMana, 6);

}

void ManaBar::setMaxMana(qint32 newMaxMana)
{
    _maxMana = newMaxMana;
}

qint32 ManaBar::mana() const
{
    return _mana;
}

void ManaBar::setMana(qint32 val)
{
    _mana = val;
}

void ManaBar::addMana(qint32 manaVal)
{
    if(_mana < _maxMana)
    {
        _mana += manaVal;
        if(_mana > _maxMana)
            _mana = _maxMana;
    }

    update();
}
