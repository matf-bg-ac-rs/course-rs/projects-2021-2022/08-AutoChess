#include "bench.h"
#include "game.h"
#include <iostream>
#include <QGraphicsScene>

Bench::Bench()
    : _color(Qt::lightGray)
{

}

void Bench::drawBench()
{
    //HACK: hardcoded position
    setPos(400, 700);

    for(auto i=0; i < 8; i++){
        benchCells[i] = new Cell(this);
        Cell* cell = benchCells[i];
        cell->moveBy(x() + Cell::cellSideLen()*i, y());
        cell->setColor(_color);
        cell->setOriginalColor(_color);
        cell->setLocation(Location::bench);
        Game::game()->addToScene(benchCells[i]);
    }

}

QRectF Bench::boundingRect() const
{
    return QRectF(_x, _y, Cell::cellSideLen()*8, Cell::cellSideLen());

}

void Bench::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    Q_UNUSED(option)

    painter->setBrush(_color);
    painter->drawRect( _x, _y, Cell::cellSideLen()*8, Cell::cellSideLen());

}
