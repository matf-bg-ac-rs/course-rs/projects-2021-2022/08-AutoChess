#ifndef LEVELINDICATOR_H
#define LEVELINDICATOR_H

#include <QGraphicsObject>
#include "utils.h"


class LevelIndicator : public QGraphicsObject
{
public:
    LevelIndicator(qint32 levelValue, QGraphicsObject* parent);

    void setImage();
    void drawParticle();
    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;

    //pixmap

    QPixmap _pixmap;
    QPixmap _scaledPixmap;



    qint32 levelValue() const;
    void setLevel(qint32 newLevelValue);
    void refresh();

private:
    qint32 _levelValue;
    QPoint _offset;
    qint32 _sideLen;

};

#endif // LEVELINDICATOR_H
