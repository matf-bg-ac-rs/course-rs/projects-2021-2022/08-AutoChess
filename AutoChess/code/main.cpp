#include "game.h"

#include <QApplication>
#include <QtGui>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QColor>
#include <QWidget>
#include <QGraphicsProxyWidget>



Unit* selectedUnit = nullptr;
Cell* selectedCell = nullptr;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Game* game = Game::game();
    game->show();
    game->mainMenu();
    return a.exec();

}
