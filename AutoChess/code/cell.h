#ifndef CELL_H
#define CELL_H

#include <QGraphicsItem>
#include <QPainter>
#include <QPoint>
#include <QString>
#include "utils.h"

class Cell : public QGraphicsObject
{
    Q_OBJECT

public:
    Cell(QGraphicsObject* parent);

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    void setColor(QColor color);
    void setOriginalColor(QColor originalColor);


    static qint32 cellSideLen() {return 96;}
    void setTaken(bool taken);
    bool isTaken();

    Location getLocation() const;
    void setLocation(Location newLocation);

signals:
    void CellClicked();

public:
    QColor _color;
    QColor _originalColor;
    bool _taken;
    Location _location;
    qint32  _col;
    qint32  _row;
};

#endif // CELL_H
