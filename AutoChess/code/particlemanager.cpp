#include "particlemanager.h"
#include <QGraphicsScene>

#include "game.h"


ParticleManager::ParticleManager()
{

    attackOffset = QPoint(4, 4);
    bloodOffset = QPoint(23, 23);
    disableOffset = QPoint(24, 24);

    attackParticle = nullptr;
    bloodParticle = nullptr;
    disableParticle = nullptr;

    receivedOffsets = std::vector<QPoint> {
        {4, 22}, {33, 22}, {76, 22},
        {4, 50}, {33, 50}, {76, 50}
    };

}

void ParticleManager::receiveParticle(ParticleType particleType, UnitType unitType, QPointF unitPos)
{
    switch(particleType) {
        case ParticleType::blood :
            if (bloodParticle == nullptr) {
                bloodParticle = new Particle(particleType, unitType);
                bloodParticle->setPos(unitPos + bloodOffset);
                Game::game()->addToScene(bloodParticle);
                bloodParticle->setVisible(true);
                bloodParticle->update();
            }
        break;
        case ParticleType::heal :
            if (bloodParticle == nullptr) {
                bloodParticle = new Particle(particleType, unitType);
                bloodParticle->setPos(unitPos + bloodOffset);
                Game::game()->addToScene(bloodParticle);
                bloodParticle->setVisible(true);
                bloodParticle->update();
            }
        break;
        case ParticleType::attack :
            if (attackParticle == nullptr) {
                attackParticle = new Particle(particleType, unitType);
                attackParticle->setPos(unitPos + attackOffset);
                Game::game()->addToScene(attackParticle);
                attackParticle->setVisible(true);
                attackParticle->update();
            }
        break;
        case ParticleType::spell :
            if (attackParticle == nullptr) {
                attackParticle = new Particle(particleType, unitType);
                attackParticle->setPos(unitPos + attackOffset);
                Game::game()->addToScene(attackParticle);
                attackParticle->setVisible(true);
                attackParticle->update();
            }
        break;
        case ParticleType::disable :
            if (disableParticle == nullptr) {
                disableParticle = new Particle(particleType, unitType);
                disableParticle->setPos(unitPos + disableOffset);
                Game::game()->addToScene(disableParticle);
                disableParticle->setVisible(true);
                disableParticle->update();
            }
        break;
        default:
            receivedParticles.push_back(new Particle(particleType, unitType));
        auto index = receivedParticles.size() -1;

        Particle* particle = receivedParticles.back();
        particle->setPos(unitPos + receivedOffsets[index]);
        Game::game()->addToScene(particle);
        particle->setVisible(true);
        particle->update();
    }
}

void ParticleManager::clearParticles()
{
    if (bloodParticle != nullptr) {
        Game::game()->removeFromScene(bloodParticle);
        delete bloodParticle;
        bloodParticle = nullptr;
    }
    if (attackParticle != nullptr) {
        Game::game()->removeFromScene(attackParticle);
        delete attackParticle;
        attackParticle = nullptr;
    }

    for (auto particle : receivedParticles) {
        Game::game()->removeFromScene(particle);
        delete particle;
    }
    receivedParticles.clear();
}

void ParticleManager::clearDisableParticles()
{
    if (disableParticle != nullptr) {
        Game::game()->removeFromScene(disableParticle);
        delete disableParticle;
        disableParticle = nullptr;
    }
}

void ParticleManager::updateDisableParticles(QPointF unitPos)
{
    if (disableParticle != nullptr) {
        disableParticle->setPos(unitPos + disableOffset);
        disableParticle->update();
    }
}
