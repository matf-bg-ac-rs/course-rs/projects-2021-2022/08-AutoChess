#include "unitpool.h"

UnitPool::UnitPool()
    : nrOfEachUnitInPool(20), nrOfTypes(7)
{
    for(auto i = 0; i < nrOfTypes; i++)
        for(auto j=0; j<nrOfEachUnitInPool; j++)
            pool.append(static_cast<UnitType>(i));
}

void UnitPool::removeFromPool(qint32 i)
{
    pool.remove(i);
}

void UnitPool::addToPool(UnitType type)
{
    pool.append(type);
}


