#include "stats.h"
#include "ui_stats.h"
#include "game.h"
#include "unitName.h"

Stats::Stats(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Stats)
{
    ui->setupUi(this);
    move(-50,-85+460+12);

    statsCell = new UICell(nullptr);
    statsCell->moveBy(220,417);
    statsCell->setColor(Qt::lightGray);
    statsCell->setOriginalColor(Qt::lightGray);

}

void Stats::drawStats()
{
    proxyParent = Game::game()->addWidget(this);
    Game::game()->addToScene(statsCell);
}

Stats::~Stats()
{
    delete ui;
}
/*
void Stats::paintEvent(QPaintEvent* event)
{

    Q_UNUSED(event);
    QPainter painter(this);
    if(!_scaledPixmap.isNull())
    {
        painter.drawPixmap(statsCell->boundingRect().toRect(), _scaledPixmap);
        qDebug("nacrtao sam");
    }
    else
        return ;
}
*/
void Stats::fillStats(Unit *selectedUnit)
{
    statsCell->_originalPixmap = selectedUnit->_originalPixmap;
    statsCell->update();
    updateStats(selectedUnit);
}

void Stats::updateStats(Unit *selectedUnit)
{
    ui->lName->setText(UnitName(*selectedUnit).name_);
    ui->lHP->setText("HP: " + QString::number(selectedUnit->_hp) + "/" + QString::number(selectedUnit->_maxHp));
    ui->lStarLVL->setText("LVL: " + QString::number(selectedUnit->_level));
    ui->lDamage->setText("Damage: " + QString::number(selectedUnit->_damage));
    ui->lMana->setText("Mana: " + QString::number(selectedUnit->_mana) + "/" + QString::number(selectedUnit->_maxMana));
    ui->lAttSpeed->setText("AttSpeed: " + QString::number(selectedUnit->attackCooldown/1000.0));
    ui->lAttRange->setText("Range: " + QString::number(selectedUnit->_range));
    ui->lSpellName->setText(selectedUnit->_spellName);
    selectedUnit->updateSpellText();
    ui->lSpellDescription->setText(selectedUnit->_spellText);
}

void Stats::clearStats()
{
    statsCell->_originalPixmap = QPixmap();
    statsCell->update();
    ui->lName->setText("/");
    ui->lStarLVL->setText("LVL:");
    ui->lHP->setText("HP:");
    ui->lDamage->setText("Damage:");
    ui->lMana->setText("Mana:");
    ui->lAttSpeed->setText("AttSpeed: ");
    ui->lAttRange->setText("Range:");
    ui->lSpellName->setText("");
    ui->lSpellDescription->setText("");
}
