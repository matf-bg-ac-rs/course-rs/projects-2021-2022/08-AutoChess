#ifndef NECROMAGE_H
#define NECROMAGE_H

#include "unit.h"
#include "utils.h"

class Necromage : public Unit
{
public:
    Necromage(Team team, quint32 level, QGraphicsItem *parent = 0);

    void drawUnit() override;
    void setHighlight(bool hl);
    bool getHighlight();
    void acceptVisit(UnitVisitor& visitor) override {visitor.visitNecromage(*this);};

    void castSpell() override;

    void giveStatsForLevel() override;

    Unit* findLowestHpAlly();

    void updateSpellText() override;
private:
    bool _highlight;
};

#endif // NECROMAGE_H

