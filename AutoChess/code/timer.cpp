#include "timer.h"
#include "ui_timer.h"
#include "game.h"
#include "unit.h"

#include <iostream>
#include <chrono>

#include "utils.h"

Timer::Timer(QWidget *parent, quint32 shopDuration, quint32 combatDuration) :
    QWidget(parent), _shopPhaseDuration(shopDuration), _combatPhaseDuration(combatDuration), _time(shopDuration), ui(new Ui::Timer)
{
    ui->setupUi(this);
    move(400+8*96 + 20,-85 - 5);
}

void Timer::drawTimer()
{
    proxyParent = Game::game()->addWidget(this);
    ui->lTime->setText(QString::number(_time));

    shopGuiTimer = new QTimer(this);
    shopGuiTimer->setInterval(Ticks::oneSecond);

    combatGuiTimer = new QTimer(this);
    combatGuiTimer->setInterval(Ticks::oneSecond);

    combatTick = new QTimer(this);
    combatTick->setInterval(Ticks::minTickInterval);

    connect(shopGuiTimer, &QTimer::timeout, this, [&]
    {
        if(_time == 1)
        {
            shopGuiTimer->stop();
//            qDebug() << "istekao shop timer";

            Game::game()->combatPhase();

        }
        _time --;
        ui->lTime->setText(QString::number(_time));
    });

    connect(combatTick, &QTimer::timeout, Game::game()->board,[&]
    {
//        auto startTime = std::chrono::high_resolution_clock::now();


        for (Unit* unit : Game::game()->board->topUnits) {
            unit->particleManager->clearParticles();
        }
        for (Unit* unit : Game::game()->board->bottomUnits) {
            unit->particleManager->clearParticles();
        }

        for (Unit* unit : Game::game()->board->topUnits) {
            unit->executeNextMove();
        }
        for (Unit* unit : Game::game()->board->bottomUnits) {
            unit->executeNextMove();
        }
        //CHRONO
//        auto stopTime = std::chrono::high_resolution_clock::now();
//        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stopTime - startTime);

//        std::cout << "Time taken by function: "
//             << duration.count() << " microseconds" << std::endl;

        if(Game::game()->unitManager->checkRoundEnd())
        {
            Game::game()->setPhase(Phase::shop);
        }
    });

    connect(combatGuiTimer, &QTimer::timeout, this, [&]
    {

        if(_time == 1 || Game::game()->getPhase() == Phase::shop)
        {
            combatTick->stop();
            combatGuiTimer->stop();
            //qDebug() << "istekao combatguitimer";

            Game::game()->serializeRoundInfo();

            Game::game()->shopPhase();

        }

        _time --;
        ui->lTime->setText(QString::number(_time));
    });

}

void Timer::updateTimerColor(Phase phase)
{
    if(phase == Phase::shop)
    {
        ui->lTime->setStyleSheet("QLabel { color:rgb(255, 255, 0); }");
    }
    else if(phase == Phase::combat)
    {
        ui->lTime->setStyleSheet("QLabel { color:rgb(255, 0, 0); }");
    }
}

void Timer::resetTime(Phase phase)
{
    if(phase == Phase::shop)
    {
        _time = _shopPhaseDuration;
    }
    else if(phase == Phase::combat)
    {
        _time = _combatPhaseDuration;
    }
}

Timer::~Timer()
{
    delete ui;
}
