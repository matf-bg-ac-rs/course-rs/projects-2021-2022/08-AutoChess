#include "elvenranger.h"
#include "game.h"

ElvenRanger::ElvenRanger(Team team, quint32 level, QGraphicsItem *parent)
    : Unit(team, level, UnitType::elvenranger, parent), _highlight(false)
{

    _originalPixmap = QPixmap(":/resources/images/elvenRanger.png");
    _hlPixmap = QPixmap(":/resources/images/elvenRangerHL.png");
    setImage();
    loadStats("ElvenRanger");
    hpBar = new HpBar(this, _team, _maxHp, _hp);
    manaBar = new ManaBar(this, _team, _maxMana, _mana);
    giveStatsForLevel();
    Game::game()->addToScene(hpBar);
    Game::game()->addToScene(manaBar);
    hpBar->setVisible(false);
    manaBar->setVisible(false);
}

void ElvenRanger::drawUnit()
{
    setZValue(ZValues::zUnit);
    Game::game()->addToScene(this);
    if(_team == Team::top && Game::game()->getGameMode() == HardMode::ON)
        hideUnit();
}

void ElvenRanger::castSpell()
{
    return;
}

void ElvenRanger::attack()
{
    if (_target != nullptr && _target->_dead)
        return;

    if(isInRange() && remainingAttackCooldown <= 0) {
        remainingAttackCooldown = attackCooldown;
    } else {
        return;
    }

    bool killed = false;
    srand (time(NULL));
    unsigned random = rand() % 100;
    if(random < 34){
        particleManager->receiveParticle(ParticleType::spell, _type, pos());
        _target->particleManager->receiveParticle(ParticleType::received, _type, _target->pos());
        _target->particleManager->receiveParticle(ParticleType::blood, _target->_type, _target->pos());
        killed = _target->takeDamage(_damage + _damage*3/4);
        damageBar->addDamageDealt(_damage + _damage*3/4);
    }
    else{
        particleManager->receiveParticle(ParticleType::attack, _type, pos());
        _target->particleManager->receiveParticle(ParticleType::received, _type, _target->pos());
        _target->particleManager->receiveParticle(ParticleType::blood, _target->_type, _target->pos());
        killed = _target->takeDamage(_damage);
        damageBar->addDamageDealt(_damage);
    }

    if (killed) {
        _target->particleManager->clearParticles();
        _target->particleManager->clearDisableParticles();
        _target = nullptr;
    }

}

void ElvenRanger::giveStatsForLevel()
{
    _maxHp = _hpPerLevel * (_level + 1);
    _hp = _maxHp;
    hpBar->setMaxHp(_maxHp);
    _spellText = "Elven Ranger has 33% chance to hit enemy for 175% damage";
    _damage = _dmgPerLevel * (_level + 1);
}
