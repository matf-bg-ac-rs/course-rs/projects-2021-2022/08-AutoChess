#include "reaper.h"
#include "game.h"


Reaper::Reaper(Team team, quint32 level, QGraphicsItem *parent)
    : Unit(team, level, UnitType::reaper, parent), _highlight(false)
{

    _hlPixmap = QPixmap(":/resources/images/reaperHL.png");
    _originalPixmap = QPixmap(":/resources/images/reaper.png");
    setImage();
    moveCooldown = 100;
    remainingMoveCooldown = 2000;
    moveCooldown = 1000;
    loadStats("Reaper");
    hpBar = new HpBar(this, _team, _maxHp, _hp);
    manaBar = new ManaBar(this, _team, _maxMana, _mana);
    giveStatsForLevel();
    Game::game()->addToScene(hpBar);
    Game::game()->addToScene(manaBar);
    hpBar->setVisible(false);
    manaBar->setVisible(false);
}

void Reaper::drawUnit()
{
    setZValue(ZValues::zUnit);
    Game::game()->addToScene(this);
    if(_team == Team::top && Game::game()->getGameMode() == HardMode::ON)
        hideUnit();
}

void Reaper::giveStatsForLevel()
{
    _maxHp = _hpPerLevel * (_level + 1);
    _hp = _maxHp;
    hpBar->setMaxHp(_maxHp);

    _bonusDmg = 0.015 * (_level + 1);
    _spellText = "Reaper will always attack unit with lowest hp, dealing (" + QString::number(_bonusDmg * 100.0, 'f', 1) + "% missing healt) as bonus damage";
    _damage = _dmgPerLevel * (_level + 1);
}


void Reaper::findTarget()
{
    auto& graph = Game::game()->board->graph;

    qint32 minHp = Limits::infHp;

    if (_target != nullptr && _target->_dead)
        _target = nullptr;

    auto& units = _team == Team::bottom ? Game::game()->board->topUnits : Game::game()->board->bottomUnits;

    for(Unit* unit : units)
    {
        if(unit->_dead)
            continue;

        if(unit->_hp < minHp && graph->getNeighbors(unit->_node).size() != 0)
        {
            minHp = unit->_hp;
            _target = unit;
        }
    }

    if (minHp == Limits::infHp)
        _target = nullptr;

}

void Reaper::getInRange()
{
    auto& graph = Game::game()->board->graph;

    if (remainingMoveCooldown > 0){
        return;
    } else {
        remainingMoveCooldown = moveCooldown;
    }

    if (_target != nullptr && _target->_dead)
        _target = nullptr;

    if (_target == nullptr)
        return;

    if (distance(_node, _target->_node) <= _range)
        return;

    //qDebug("PROSAO PROVERU GETINRANGEA");
    //mogu unapred uniti rezervisati polje na koje ce da stanu (mozda)

    auto neighbors = graph->getNeighbors(_target->_node);
    if (neighbors.size() == 0)
        return;

    Node minDistNeighbor = NotFound;

    for (auto neighbor : neighbors) {
        if (minDistNeighbor == NotFound) {
            minDistNeighbor = neighbor;
        } else {
            if (distance(_node, neighbor) < distance(_node, minDistNeighbor))
                minDistNeighbor = neighbor;
        }
    }

    Node nextNode = minDistNeighbor;

    moveStep(nextNode);
}

void Reaper::attack()
{
    if (_target != nullptr && _target->_dead)
        return;

    if(isInRange() && remainingAttackCooldown <= 0) {
        remainingAttackCooldown = attackCooldown;
    } else {
        return;
    }
    bool killed = false;
    particleManager->receiveParticle(ParticleType::attack, _type, pos());
    _target->particleManager->receiveParticle(ParticleType::received, _type, _target->pos());
    _target->particleManager->receiveParticle(ParticleType::blood, _target->_type, _target->pos());

    killed = _target->takeDamage(_damage + (_target->_maxHp - _target->_hp)*_bonusDmg);
    damageBar->addDamageDealt(_damage + (_target->_maxHp - _target->_hp)*_bonusDmg);

    if (killed) {
        _target->particleManager->clearParticles();
        _target->particleManager->clearDisableParticles();
        _target = nullptr;
    }

}

void Reaper::castSpell()
{
    return;
}
