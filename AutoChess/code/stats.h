#ifndef STATS_H
#define STATS_H

#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsProxyWidget>
#include <QGraphicsPixmapItem>
#include <QString>
#include "uicell.h"
#include "unit.h"

namespace Ui {
class Stats;
}

class Stats : public QWidget
{
    Q_OBJECT

public:
    explicit Stats(QWidget *parent = nullptr);
    ~Stats();
    UICell *statsCell;
    QGraphicsProxyWidget *proxyParent;
    void updateStats(Unit *selectedUnits);
    void drawStats();
public slots:
    void fillStats(Unit *selectedUnit);
    void clearStats();
private:
    Ui::Stats *ui;
};

#endif // STATS_H
