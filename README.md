# Project AutoChess

AutoChess je strateški autobattler u kom je na igraču da napravi bolji tim i nadjača neprijatelja. Igrač kupuje heroje iz prodavnice i stavlja na tablu za vreme Shop faze. Po isteku vremena igrač ne može da menja stanje table, već samo da gleda kako bitka teče. Likovi na tabli se sami kreću i tuku, sve dok jedan igrac ne ostane bez heroja ili dok ne istekne tajmer. Posle bitke počinje sledeća runda gde igrač opet mora da ojača svoj tim kako bi dorastao izazovu koji svaka nova runda nosi. Posle 5 rundi se igra završava i aplikacija pamti ishod igre i piše ga u gameSummary.json fajl. 



## Developers

- [Ivan Basarab, 308/2017](https://gitlab.com/blasarab)
- [Aleksa Veselić, 317/2017](https://gitlab.com/Vessely)
- [Petar Dinić, 90/2017](https://gitlab.com/ptrdnc)

## Demo snimak

[Demo snimak](https://www.youtube.com/watch?v=PlebseOUrZA)

## Bildovanje i pokretanje izvornog koda

Nakon kloniranja projekta, pozicionirati se unutar njega
```bash
mkdir build
cd build
cmake -g "Unix Makefiles" -DCMAKE_PREFIX_PATH="/usr/lib64/cmake" ..
make AutoChess
```
